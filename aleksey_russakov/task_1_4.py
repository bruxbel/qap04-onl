a, b = 4, 6

arithm_average = (a + b)/2
geom_average = (a*b)**(1/2)

print(f"", arithm_average, "\n", geom_average)